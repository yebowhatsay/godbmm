# README #



### What is this repository for? ###

* Quick summary
    Package godbmm provides database Read(), Update() for sql server
    This is mainly for personal use. 
* Version
    0.5


### How do I get set up? ###

* Summary of set up
    git clone git@bitbucket.org:yebowhatsay/godbmm.git
* Configuration
* Dependencies
    Read http://go-database-sql.org/retrieving.html for help on how to query sql DBs
* Deployment instructions
    import "bitbucket.org/yebowhatsay/godbmm"

    then
        //DbConfg is Config interface, lg is Logger interface and eml is Mailer interface
        // d is debug param which is a bool. True will log.Print the sql statement that is being executed
        // for debugging purpose. 
        // inject the config file, logger and mailer here
        // do this wherever your app needs a sql server database
        sqlDB := db.New(DbCfg, lg, eml, d)

  Config interface is as follows
  type Config interface {
        DbUsername() string
        DbPassword() string
        DbHostname() string
        DbInstance() string
        DbName() string
        DbType() string 
    }
    //Hopefully, this is self explanatory. Make sure the methods return username, password, etc.

    Mailer interface is as follows
    type Mailer interface {
        Info(string)
        Error(error)
    }

    This emails either info or error messages 

    Logger interface is as follows 

    type Logger interface {
        Info(args ...interface{})
        Infof(s string, args ...interface{})
        Error(args ...interface{})
        Fatal(args ...interface{})
        Debug(args ...interface{})
    }
    
### Notes
$ before @ makes the query string not make Named Args for @r
for e.g. query := `DECLARE $@r int EXEC $@r = BP_IsAppointmentBooked @a , @b , @c PRINT $@r`    

### Contribution guidelines ###

Feel free to fork, contribute or give feedback

### Who do I talk to? ###

* yebowhatsay@bitbucket or yebowhat@github
