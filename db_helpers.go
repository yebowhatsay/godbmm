package db

import (
	"database/sql"
	"strings"
)

//remoteAtChar removes the first character if it is either @
func removeAtChar(word string) string {
	if word[:1] == "@" { // checks if @ is the first char
		return word[1:]
	}
	return word
}

//removeRoundBrackets removes round brackets from string argument
func removeRoundBrackets(word string) string {
	if word[:1] == "(" && word[len(word)-1:] == ")" {
		return word[1 : len(word)-1] //removes the round brackets around the word, if present
	}
	return word
}

//remoteTrailingComma removes the last character if it is a comma
func removeTrailingComma(word string) string {
	if word[len(word)-1:] == "," { // checks if , is the last char
		return word[:len(word)-1] //remove the last char
	}
	return word
}

//removeDollar. removes $ sign from variables.
//for e.g., DECLARE $@r int EXEC $@r = BP_IsAppointmentBooked $a , $b , $c PRINT $@r. Need to pass in @r without it being part of NamedArgs.
func removeDollar(q string) (string) {
	return strings.ReplaceAll(q, "$", "")
}

//getParams returns slice of interface{}. actually returns slice of sql.NamedArg
//get statement.Exec only accepts interface{} as param; hence, defined here as such
func getParams(query string, params ...interface{}) []interface{} {
	var namedParams []interface{}
	y := 0
	words := strings.Fields(query) // gets slice of string words from query string
	var parameter []string
	for _, word := range words {
		if len(word) == 1 { //continue with loop if wordRunes is a single rune; e.g. "=" if a ","
			continue
		}

		//append to parameter if word stars with @ char
		// if word[:1] == "@" || word[1:2] == "@" {
		if word[:1] == "@" {
			parameter = append(parameter, removeTrailingComma(removeAtChar(word)))
			y++
		}

	}

	for x, param := range params {
		namedParams = append(namedParams, sql.Named(parameter[x], param))
	}
	return namedParams
}