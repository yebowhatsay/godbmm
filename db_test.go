package db

import (
	"database/sql"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

////dbCfg empty struct
//type dbMockCfg struct{}
//
////DbCfg instance of dbCfg
//var DbMockCfg dbMockCfg
//
////DbUsername returns dbUsername string
//func (d dbMockCfg) DbUsername() string {
//	return ""
//}
//
////DbPort returns dbPort uint32
//func (d dbMockCfg) DbPort() uint32 {
//	return 0
//}
//
////DbPassword returns dbPassword
//func (d dbMockCfg) DbPassword() string {
//	return ""
//}
//
////DbHostname returns dbHostname
//func (d dbMockCfg) DbHostname() string {
//	return ""
//}
//
////DbInstance returns dbInstance
//func (d dbMockCfg) DbInstance() string {
//	return ""
//}
//
////DbName returns dbName
//func (d dbMockCfg) DbName() string {
//	return ""
//}
//
////DbName returns dbType
//func (d dbMockCfg) DbType() string {
//	return ""
//}

func Test_waitForConnection(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	_ = mock
	type args struct {
		db       *sql.DB
		duration time.Duration
	}
	tests := []struct {
		name    string
		arg     args
		wantErr bool
	}{
		{
			arg: args{
				db:       db,
				duration: time.Minute,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotErr := waitForConnection(tt.arg.db, tt.arg.duration); (gotErr != nil) != tt.wantErr {
				t.Errorf("waitForConnection() got = %v, want %v", gotErr, tt.wantErr)
			}
		})
	}

}

func Test_spExec(t *testing.T) {
	d := &DB{
		cfg: config{
			dbType: "mysql",
		},
	}
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	d.dber = func(*DB) (*sql.DB, error) {
		return db, nil
	}
	_ = mock
	type args struct {
		query  string
		db     *DB
		params []interface{}
	}
	tests := []struct {
		name    string
		arg     args
		want    sql.Result
		wantErr bool
	}{
		{
			arg: args{
				query: ` EXEC BP_GetFreeAppointments @a , @b `,
				db:    d,
				params: []interface{}{
					"mock",
					34,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectPrepare(tt.arg.query)
			mock.ExpectQuery(tt.arg.query).WithArgs("mock", 34).WillReturnRows(&sqlmock.Rows{})
			got, gotErr := spExec(d, tt.arg.query, tt.arg.params...)
			if (gotErr != nil) != tt.wantErr {
				t.Errorf("Insert() gotErr = %v, wantErr %v", gotErr, tt.wantErr)
			}
			if got == nil {
				t.Errorf("Insert() got = %#v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Insert(t *testing.T) {
	d := &DB{
		cfg: config{
			dbType: "mysql",
		},
	}
	db, mock, err := sqlmock.New()
	mock.ExpectBegin()

	//mock.ExpectExec(`EXEC BP_GetFreeAppointments`).WillReturnResult(sqlmock.NewResult(1, 1))
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	d.dber = func(d *DB) (*sql.DB, error) {
		return db, nil
	}
	_ = mock
	type args struct {
		query  string
		db     *DB
		params []interface{}
	}
	tests := []struct {
		name    string
		arg     args
		want    sql.Result
		wantErr bool
	}{
		{
			arg: args{
				query: ` EXEC BP_GetFreeAppointments @a , @b `,
				db:    d,
				params: []interface{}{
					"mock",
					34,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectPrepare(tt.arg.query)
			mock.ExpectExec(tt.arg.query).WithArgs("mock", 34).WillReturnResult(sqlmock.NewResult(1, 1))
			mock.ExpectCommit()
			got, gotErr := insert(d, tt.arg.query, tt.arg.params...)
			if (gotErr != nil) != tt.wantErr {
				t.Errorf("Insert() gotErr = %v, wantErr %v", gotErr, tt.wantErr)
			}
			if got == nil {
				t.Errorf("Insert() got = %#v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Update(t *testing.T) {
	d := &DB{
		cfg: config{
			dbType: "mysql",
		},
	}
	db, mock, err := sqlmock.New()
	mock.ExpectBegin()

	//mock.ExpectExec(`EXEC BP_GetFreeAppointments`).WillReturnResult(sqlmock.NewResult(1, 1))
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	d.dber = func(d *DB) (*sql.DB, error) {
		return db, nil
	}
	_ = mock
	type args struct {
		query  string
		db     *DB
		params []interface{}
	}
	tests := []struct {
		name    string
		arg     args
		want    sql.Result
		wantErr bool
	}{
		{
			arg: args{
				query: ` EXEC BP_GetFreeAppointments @a , @b `,
				db:    d,
				params: []interface{}{
					"mock",
					34,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectPrepare(tt.arg.query)
			mock.ExpectExec(tt.arg.query).WithArgs("mock", 34).WillReturnResult(sqlmock.NewResult(1, 1))
			mock.ExpectCommit()
			got, gotErr := d.Update(tt.arg.query, tt.arg.params...)
			if (gotErr != nil) != tt.wantErr {
				t.Errorf("Update() gotErr = %v, wantErr %v", gotErr, tt.wantErr)
			}
			if got == 0 {
				t.Errorf("Update() got = %#v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Create(t *testing.T) {
	d := &DB{
		cfg: config{
			dbType: "mysql",
		},
	}
	db, mock, err := sqlmock.New()
	mock.ExpectBegin()

	//mock.ExpectExec(`EXEC BP_GetFreeAppointments`).WillReturnResult(sqlmock.NewResult(1, 1))
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	d.dber = func(d *DB) (*sql.DB, error) {
		return db, nil
	}
	_ = mock
	type args struct {
		query  string
		db     *DB
		params []interface{}
	}
	tests := []struct {
		name    string
		arg     args
		want    sql.Result
		wantErr bool
	}{
		{
			arg: args{
				query: ` EXEC BP_GetFreeAppointments @a , @b `,
				db:    d,
				params: []interface{}{
					"mock",
					34,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectPrepare(tt.arg.query)
			mock.ExpectExec(tt.arg.query).WithArgs("mock", 34).WillReturnResult(sqlmock.NewResult(1, 1))
			mock.ExpectCommit()
			got, gotErr := d.Create(tt.arg.query, tt.arg.params...)
			if (gotErr != nil) != tt.wantErr {
				t.Errorf("Create() gotErr = %v, wantErr %v", gotErr, tt.wantErr)
			}
			if got == 0 {
				t.Errorf("Create() got = %#v, want %v", got, tt.want)
			}
		})
	}

}

func Test_Read(t *testing.T) {
	d := &DB{
		cfg: config{
			dbType: "mysql",
		},
	}
	db, mock, err := sqlmock.New()


	//mock.ExpectExec(`EXEC BP_GetFreeAppointments`).WillReturnResult(sqlmock.NewResult(1, 1))
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	d.dber = func(d *DB) (*sql.DB, error) {
		return db, nil
	}
	_ = mock
	type args struct {
		query  string
		db     *DB
		params []interface{}
	}
	tests := []struct {
		name    string
		arg     args
		want    sql.Result
		wantErr bool
	}{
		{
			arg: args{
				query: ` EXEC BP_GetFreeAppointments @a , @b `,
				db:    d,
				params: []interface{}{
					"mock",
					34,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mock.ExpectPrepare(tt.arg.query)
			mock.ExpectQuery(tt.arg.query).WithArgs("mock", 34).WillReturnRows(&sqlmock.Rows{})
			//mock.ExpectCommit()
			got, gotErr := d.Read(tt.arg.query, tt.arg.params...)
			if (gotErr != nil) != tt.wantErr {
				t.Errorf("Read() gotErr = %v, wantErr %v", gotErr, tt.wantErr)
			}
			if got == nil {
				t.Errorf("Read() got = %#v, want %v", got, tt.want)
			}
		})
	}

}
