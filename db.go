package db

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"

	"net/url"
	"time"

	_ "github.com/denisenkom/go-mssqldb" // required for sqlserver or mssql
	_ "github.com/go-sql-driver/mysql"   // required for mysql. init function of this package is called
)

//DB is type sql.DB
type DB struct {
	cfg config
	MaxIdleCx int
	dber dbGetter
}

type dbGetter func(*DB) (*sql.DB, error)

type config struct {
	dbUsername string
	dbPassword string
	dbHostname string
	dbPort     uint32
	dbInstance string
	dbName     string
	dbType     string
}

//Config interface for inj of config
type Config interface {
	DbUsername() string
	DbPassword() string
	DbHostname() string
	DbPort() uint32
	DbInstance() string
	DbName() string
	DbType() string
}

//New constructor for *DB
func New(c Config) *DB {

	db := DB{}
	db.cfg = config{
		dbUsername: c.DbUsername(),
		dbPassword: c.DbPassword(),
		dbHostname: c.DbHostname(),
		dbPort:     c.DbPort(),
		dbInstance: c.DbInstance(),
		dbName:     c.DbName(),
		dbType:     c.DbType(),
	}
	db.MaxIdleCx = 4
	db.dber = getDb
	return &db
}

//Create creates new db rows and returns number of rows affected and/or error
func (d *DB) Create(query string, params ...interface{}) (recordsAffected int64, err error) {
	_, err = insert(d, query, params...)
	if err != nil {
		return recordsAffected, errors.Wrap(err, "d.Insert")
	}
	//assuming that if no error is returned, create might have gone ahead.
	//this may be incorrect, but lastInsertID does not work in sql server. needs fixing.
	//also, rowsAffected from result of insert gives incorrect number of rows inserted!
	return 1, nil
}

//Update updates db rows and returns number of rows affected and/or error
func (d *DB) Update(query string, params ...interface{}) (recordsAffected int64, err error) {
	res, err := insert(d, query, params...)
	if err != nil {
		return recordsAffected, errors.Wrap(err, "db.Update")
	}
	// warning: rowsAffected from result of insert gives incorrect number of rows inserted!
	recordsAffected, err = res.RowsAffected()
	if err != nil {
		return recordsAffected, errors.Wrap(err, "res.RowsAffected")
	}
	return
}



//insert func to create/update records, but this could be used to exec any db query that doesn't return rows
func insert(d *DB, query string, params ...interface{}) (res sql.Result, err error) {
	db, err := d.dber(d)
	if err != nil {
		return nil, errors.Wrap(err, "Insert->d.getDb")
	}

	defer db.Close()
	namedParams := getParams(query, params...)
	tx, err := db.Begin()
	if err != nil {
		return nil, errors.Wrap(err, "Insert->db.Begin")
	}

	prepStmt, err := tx.Prepare(query)
	if err != nil {
		return nil, errors.Wrap(err, "Insert->tx.Prepare")
	}

	res, err = prepStmt.Exec(namedParams...)
	if err != nil {
		return nil, errors.Wrap(err, "Insert->prepStmt.Exec")
	}

	err = prepStmt.Close()
	if err != nil {
		return nil, errors.Wrap(err, "Insert->prepStmt")
	}
	err = tx.Commit()
	if err != nil {
		return nil, errors.Wrap(err, "Insert->tx.Commit")
	}

	return
}



//Read returns db rows or error
//if mysql: query should be of type "SELECT * FROM album WHERE id = ?"
//if sqlserver: query should be of type "SELECT * FROM album WHERE id = ?id"
func (d *DB) Read(query string, params ...interface{}) (rows *sql.Rows, err error) {
	rows, err = spExec(d, query, params...)
	return
}

//SPExec executes stored procedures for mysql/mssql databases
//if mysql: query should be of type "SELECT * FROM album WHERE id = ?"
//if sqlserver: query should be of type "SELECT * FROM album WHERE id = ?id"
func spExec(d *DB, query string, params ...interface{}) (rows *sql.Rows, err error) {
	db, err := d.dber(d)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	switch t := d.cfg.dbType; t {
	case "mssql", "sqlserver":
		return execMSSQL(db, query, params...)
	case "mysql":
		return execMYSQL(db, query, params...)
	default:
		return nil, errors.New("db type not set")
	}
}

//execMYSL uses prepared statements
func execMYSQL(db *sql.DB, query string, params ...interface{}) (rows *sql.Rows, err error) {
	stmt, err := db.Prepare(query)
	if err != nil {
		return
	}
	return stmt.Query(params...)
}

//execMSSQL uses named arguments
func execMSSQL(db *sql.DB, query string, params ...interface{}) (rows *sql.Rows, err error) {
	//getParams(query, params...) gets Params as slice of interfaces in right format
	return db.Query(removeDollar(query), getParams(query, params...)...)
}

//getDb returns pointer to sql.DB
func getDb(d *DB) (db *sql.DB, err error) {

	switch t := d.cfg.dbType; t {
	case "mssql", "sqlserver":
		db, err = getMSSQLDb(d)
	case "mysql":
		db, err = getMYSQLDb(d)
	default:
		return nil, errors.New("db type not set")
	}

	db.SetMaxIdleConns(d.MaxIdleCx)

	//blocks till connection is established
	err = waitForConnection(db, time.Minute)
	if err != nil {
		return
	}

	return

}

func getMYSQLDb(d  *DB) (*sql.DB, error) {

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", d.cfg.dbUsername, d.cfg.dbPassword, d.cfg.dbHostname, d.cfg.dbPort, d.cfg.dbName))
	if err != nil {
		//d.MailAndLog(err)
		return db, err
	}
	return db, nil
}

func getMSSQLDb(d  *DB) (*sql.DB, error) {
	query := url.Values{}
	query.Add("database", d.cfg.dbName)
	query.Add("encrypt", "true") // true|disable|false For sqlserver 2008 R2 ensure Service Pack 2 is installed else true will not work
	query.Add("TrustServerCertificate", "true")
	query.Add("log", "1") //1: log errors, 32: log transaction begin/end

	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(d.cfg.dbUsername, d.cfg.dbPassword),
		Host:   fmt.Sprintf("%s:%d", d.cfg.dbHostname, d.cfg.dbPort),
		// Host:     fmt.Sprintf("%s", d.cfg.dbHostname),
		// Path:     d.cfg.dbInstance, // if connecting to an instance instead of a port. Can't use both port and instance.
		RawQuery: query.Encode(),
	}

	db, err := sql.Open("sqlserver", u.String())
	if err != nil {
		//d.MailAndLog(err)
		return nil, err
	}
	return db, nil
}

//waitForConnection if Pinging returns error, keep trying after a set interval. only allow
//program to proceed once Pinging is successful. Avoids panic when Row.Next tries to connect
//to database once connection has been lost
func waitForConnection(db *sql.DB, timeToWait time.Duration) error {
	ti := time.Now()
	for {
		b, err := isAlive(db)
		if err != nil {
			return err
		}
		if b {
			return nil
		}
		time.Sleep(10 * time.Second)
		if time.Now().After(ti.Add(timeToWait)) {
			return errors.New("db connection timed out")
		}
	}
}

//IsAlive check if database connection is active or not
func isAlive(db *sql.DB) ( bool, error) {
	// Send a ping to make sure the database connection is alive.
	if err := db.Ping(); err != nil {
			return false, errors.Wrap(err, "db.Ping")
	}
	return true, nil
}