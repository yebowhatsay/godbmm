package db

import (
	"database/sql"
	"reflect"
	"testing"
)

func TestRemoveRoundBrackets(t *testing.T) {
	shouldBeRemoved := map[string]string{
		"(hello)": "hello",
		"haha":    "haha",
		"(mouse)": "mouse",
	}

	shouldStaySame := map[string]string{
		"hello)": "hello",
		"haha":   "haha",
		"(mouse": "mouse",
	}

	for k, v := range shouldBeRemoved {
		if s := removeRoundBrackets(k); s != v {
			t.Errorf("wanted '%s', have '%s'", v, s)
		}
	}

	for k, v := range shouldStaySame {
		if s := removeRoundBrackets(k); s != k {
			t.Errorf("wanted '%s', have '%s'", v, s)
		}
	}
}

func TestRemoveAtChar(t *testing.T) {
	strs := map[string]string{
		"@(hello)": "(hello)",
		"@haha":    "haha",
		"@(mouse":  "(mouse",
	}

	for k, v := range strs {
		if s := removeAtChar(k); s != v {
			t.Errorf("wanted '%s', have '%s'", v, s)
		}
	}

}

func TestRemoveTrailingComma(t *testing.T) {
	shouldBeRemoved := map[string]string{
		"hello,": "hello",
		"a,":     "a",
		"mouse,": "mouse",
	}

	shouldStaySame := map[string]string{
		"hello": "hello",
		"haha":  "haha",
		"mouse": "mouse",
	}

	for k, v := range shouldBeRemoved {
		if s := removeTrailingComma(k); s != v {
			t.Errorf("wanted '%s', have '%s'", v, s)
		}
	}

	for k, v := range shouldStaySame {
		if s := removeTrailingComma(k); s != k {
			t.Errorf("wanted '%s', have '%s'", v, s)
		}
	}
}

func Test_removeDollar(t *testing.T) {
	tests := []struct {
		name string
		arg  string
		want string
	}{
		{
			arg: "asdf$",
			want: "asdf",
		},
		{
			arg: "$",
			want: "",
		},
		{
			arg: "$a",
			want: "a",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := removeDollar(tt.arg); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("removeDollar(t *testing.T) {() got = %v, want %v", got, tt.want)
			}
		})
	}

}

func TestGetParams(t *testing.T) {
	type NamedArg struct {
		Name  string
		Value interface{}
	}
	strs := map[string][]NamedArg{
		"update x where @hello @t @animal": {NamedArg{"hello", 5}, NamedArg{"t", "test"}, NamedArg{"animal", "cat"}},
	}

	for k, v := range strs {
		p := getParams(k, 5, "test", "cat")

		//p is a slice of sql.NamedArg
		for x, s := range p {

			switch s.(type) {
			case sql.NamedArg:

				n := s.(sql.NamedArg)
				if n.Name != v[x].Name {
					t.Errorf("wanted '%s', have '%s'", v[x].Name, n.Name)
				}

				if n.Value != v[x].Value {
					t.Errorf("wanted '%s', have '%s'", v[x].Value, n.Value)
				}

			}

		}

	}

}